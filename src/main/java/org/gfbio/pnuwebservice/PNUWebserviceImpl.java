package org.gfbio.pnuwebservice;



import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.gfbio.config.WSConfiguration;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.AllBroaderResultSetEntry;
import org.gfbio.resultset.CapabilitiesResultSetEntry;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.HierarchyResultSetEntry;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.gfbio.util.YAMLConfigReader;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.logging.LoggingFeature;


/**
 * Webservice implementation in the GFBio Terminology Service. This class implements all necessary
 * methods for the TS endpoints according to the WSInterface defined in the gfbioapi project.
 * 
 * @author nkaram <A HREF="mailto:naouel.karam@fu-berlin.de">Naouel Karam</A>
 * 
 */
public class PNUWebserviceImpl implements WSInterface {
  private static final Logger LOGGER = Logger.getLogger(PNUWebserviceImpl.class);

  private WSConfiguration pnuConfig;

  private HttpAuthenticationFeature authFilter;


  private String uri;

  private Client client;

  public PNUWebserviceImpl() {
    PNUProperties props = PNUProperties.getInstance();
    uri = props.getUri();

    // authenticate the client
    authFilter = HttpAuthenticationFeature.basic(props.getUsername(), props.getPassword());
    client = ClientBuilder.newClient();
    client.register(authFilter);
    client.register(new LoggingFeature());
    client.register(JacksonFeature.class);

    YAMLConfigReader reader = YAMLConfigReader.getInstance();
    pnuConfig = reader.getWSConfig("PNU");

    LOGGER.info("a PNU webservice is ready to use");
    LOGGER.info("URI loaded: " + uri);
  }

  @Override
  public String getAcronym() {
    return pnuConfig.getAcronym();
  }

  @Override
  public String getDescription() {
    return pnuConfig.getDescription();
  }

  @Override
  public String getName() {
    return pnuConfig.getName();
  }

  @Override
  public String getURI() {
    return uri;
  }

  @Override
  public List<String> getDomains() {
    List<String> l = new ArrayList<String>();
    for (Domains d : pnuConfig.getWsDomains()) {
      l.add(d.name());
    }
    return l;
  }

  @Override
  public String getCurrentVersion() {

    return pnuConfig.getVersion();
  }

  /**
   * Implements the search method of the WSInterface Searches for names and common names in the
   * database based on the match type "exact" or "included"
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> search(String query, String match_type) {
    try {
      // WebResource webResource = client.resource(uri + "taxon/" + query.replace(" ",
      // "%20"));

      // WebTarget webResource = buildRequest("taxon/", query.replace(" ", "%20"));

      Response response = buildRequest("taxon/", query.replace(" ", "%20"));
      if (response.getStatus() != 200) {
        LOGGER.info(response.getStatusInfo());
        throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
      }
      GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("pnu");
      JsonReader jsonReader =
          Json.createReader(new StringReader(response.readEntity(String.class)));
      JsonObject jsonObject = jsonReader.readObject();
      JsonArray array = jsonObject.getJsonArray("results");
      JsonArray correctNameArray;
      JsonObject jsonObjectResult;

      for (int i = 0; i < array.size(); i++) {
        jsonObjectResult = array.getJsonObject(i);
        SearchResultSetEntry e = new SearchResultSetEntry();
        String label;
        label = jsonObjectResult.getString("label");
        if (match_type.equals(SearchTypes.exact.name())) {
          if (!label.toLowerCase().trim().equals(query.toLowerCase().trim()))
            continue;
        }
        // If the correct name is given we replace the result by it and put the old
        // result as synonym
        if (jsonObjectResult.containsKey("correct_name")) {
          JsonValue correctNameValue = jsonObjectResult.get("correct_name");
          if (!correctNameValue.toString().equals("null")) {
            correctNameArray = jsonObjectResult.getJsonArray("correct_name");
            if (correctNameArray.size() > 0) {
              // WebResource webResourceTerm = client.resource(correctNameArray.getString(0));
              // ClientResponse responseTerm =
              // webResourceTerm.accept("application/json").get(ClientResponse.class);
              Response responseTerm = client.target(correctNameArray.getString(0))
                  .request(MediaType.APPLICATION_JSON).get();

              if (responseTerm.getStatus() != 200) {
                throw new RuntimeException(
                    "Failed : HTTP error code : " + responseTerm.getStatus());
              }
              JsonReader jsonReaderTerm =
                  Json.createReader(new StringReader(responseTerm.readEntity(String.class)));
              jsonObjectResult = jsonReaderTerm.readObject();
              e.setSynonym(label);
              label = jsonObjectResult.getString("label");
            }
          }
        }
        e.setLabel(jsonObjectResult.getString("label"));
        e.setRank(jsonObjectResult.getString("taxon"));
        e.setKingdom(jsonObjectResult.getString("regio"));
        e.setUri(jsonObjectResult.getString("url"));
        e.setSourceTerminology(getAcronym());
        if (jsonObjectResult.containsKey("pnu_no")) {
          int pnuNo = jsonObjectResult.getInt("pnu_no");
          e.setExternalID(String.valueOf(pnuNo));
        }
        e.setInternal("false");
        rs.addEntry(e);
      }
      return rs;
    } catch (Exception ex) {
      LOGGER.error("API exception in PNU search: " + ex.getMessage());
      return new GFBioResultSet<SearchResultSetEntry>("pnu");
    }
  }

  /**
   * Implements the getAllBroader method of the WSInterface Gets all broader terms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<AllBroaderResultSetEntry> getAllBroader(String term_uriORexternalID) {
    GFBioResultSet<AllBroaderResultSetEntry> rs =
        new GFBioResultSet<AllBroaderResultSetEntry>("pnu");
    try {
      rs.addWarning("The requested service is not available for the given terminology");
      return rs;
    } catch (Exception e) {
      LOGGER.error("API exception in PNU allbroader");
      return rs;
    }
  }

  /**
   * Implements the getHierarchy method of the WSInterface Gets the broader hierarchy of a taxon
   * given its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<HierarchyResultSetEntry> getHierarchy(String term_uriORexternalID) {
    GFBioResultSet<HierarchyResultSetEntry> rs = new GFBioResultSet<HierarchyResultSetEntry>("pnu");
    try {
      rs.addWarning("The requested service is not available for the given terminology");
      return rs;
    } catch (Exception e) {
      LOGGER.error("API exception in PNU hierarchy");
      return rs;
    }
  }

  /**
   * Implements the getMetadata method of the WSInterface Gets the metadata information about the
   * webservice
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<MetadataResultSetEntry> getMetadata() {
    GFBioResultSet<MetadataResultSetEntry> rs = new GFBioResultSet<MetadataResultSetEntry>("pnu");
    try {
      MetadataResultSetEntry e = new MetadataResultSetEntry();
      e.setAcronym(getAcronym());
      e.setName(getName());
      e.setVersion(getCurrentVersion());
      e.setDescription(getDescription());
      e.setKeywords(pnuConfig.getKeywords());
      e.setContact(pnuConfig.getContact());
      e.setUri(getURI());
      e.setContribution(pnuConfig.getContribution());
      e.setStorage(pnuConfig.getStorage());
      ArrayList<String> domainUris = new ArrayList<String>();
      for (Domains d : pnuConfig.getWsDomains()) {
        domainUris.add(d.getUri());
      }
      e.setDomain(domainUris);
      rs.addEntry(e);
      return rs;
    } catch (Exception e) {
      LOGGER.error("API exception in PNU metadata");
      return rs;
    }
  }

  /**
   * Implements the getSynonyms method of the WSInterface Gets the list of synonyms of a taxon given
   * its URI or External ID
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SynonymsResultSetEntry> getSynonyms(String term_uriORexternalID) {
    GFBioResultSet<SynonymsResultSetEntry> rs = new GFBioResultSet<SynonymsResultSetEntry>("pnu");
    try {
      // WebResource webResource = client.resource(uri + "species/" +
      // term_uriORexternalID + "/");
      Response response = buildRequest("species/", term_uriORexternalID);
      // ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
      if (response.getStatus() != 200) {
        throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
      }
      SynonymsResultSetEntry e = new SynonymsResultSetEntry();
      JsonReader jsonReader =
          Json.createReader(new StringReader(response.readEntity(String.class)));
      JsonObject jsonObject = jsonReader.readObject();
      JsonArray synonymsArray = jsonObject.getJsonArray("synonyms");
      ArrayList<String> synonyms = new ArrayList<String>();
      String synonym;
      for (int i = 0; i < synonymsArray.size(); i++) {
        JsonObject jo = synonymsArray.getJsonObject(i);

        synonym = jo.getString("synonym").replace("</i>", "").replace("<i>", "");
        synonyms.add(synonym);

      }
      e.setSynonyms(synonyms);
      rs.addEntry(e);
      return rs;
    } catch (Exception e) {
      LOGGER.error("API exception in PNU synonyms: " + e.getMessage());
      return rs;
    }
  }

  /**
   * Implements the getTermInfosProcessed method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<SearchResultSetEntry> getTermInfosProcessed(String term_uri) {
    GFBioResultSet<SearchResultSetEntry> rs = new GFBioResultSet<SearchResultSetEntry>("pnu");
    try {
      if (term_uri.startsWith("http:"))
        term_uri = term_uri.replace("http", "https");

      if (!term_uri.startsWith("http"))
        term_uri = uri + "species/" + term_uri + "/";

      // WebResource webResource = client.resource(term_uri);
      // ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
      Response response = client.target(term_uri).request(MediaType.APPLICATION_JSON).get();

      if (response.getStatus() != 200) {
        throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
      }
      SearchResultSetEntry e = new SearchResultSetEntry();
      JsonReader jsonReader =
          Json.createReader(new StringReader(response.readEntity(String.class)));
      JsonObject jsonObject = jsonReader.readObject();
      e.setLabel(jsonObject.getString("label"));
      e.setRank(jsonObject.getString("taxon"));
      e.setKingdom("regio");
      e.setUri(jsonObject.getString("url"));
      e.setSourceTerminology("PNU");
      if (jsonObject.containsKey("pnu_no")) {
        int pnuNo = jsonObject.getInt("pnu_no");
        e.setExternalID(String.valueOf(pnuNo));
      }
      if (jsonObject.containsKey("synonyms")) {
        JsonArray synonymsArray = jsonObject.getJsonArray("synonyms");
        String synonymsString = "";
        String synonym;
        for (int i = 0; i < synonymsArray.size(); i++) {
          JsonObject jo = synonymsArray.getJsonObject(i);
          synonym = jo.getString("synonym").replace("</i>", "").replace("<i>", "");
          synonymsString = synonymsString + " " + synonym;
        }
        e.setSynonym(synonymsString.trim());
      }
      rs.addEntry(e);
      return rs;
    } catch (Exception ex) {
      LOGGER.error("API exception in PNU term infos: " + ex.getMessage());
      return rs;
    }
  }

  /**
   * Implements the getTermInfosOriginal method of the WSInterface Gets the informations of a taxon
   * given its URI with the original attributes
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<TermOriginalResultSetEntry> getTermInfosOriginal(String term_uri) {
    GFBioResultSet<TermOriginalResultSetEntry> rs =
        new GFBioResultSet<TermOriginalResultSetEntry>("pnu");
    try {
      if (term_uri.startsWith("http:"))
        term_uri = term_uri.replace("http", "https");

      if (!term_uri.startsWith("http"))
        term_uri = uri + "species/" + term_uri + "/";
      // WebResource webResource = client.resource(term_uri);
      // ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
      Response response = client.target(term_uri).request(MediaType.APPLICATION_JSON).get();

      if (response.getStatus() != 200) {
        throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
      }
      TermOriginalResultSetEntry e = new TermOriginalResultSetEntry();
      JsonReader jsonReader =
          Json.createReader(new StringReader(response.readEntity(String.class)));
      JsonObject jsonObject = jsonReader.readObject();
      e.setOriginalTermInfo("url", jsonObject.getString("url"));
      e.setOriginalTermInfo("label", jsonObject.getString("label"));
      if (jsonObject.containsKey("pnu_no")) {
        int pnuNo = jsonObject.getInt("pnu_no");
        e.setOriginalTermInfo("pnu_no", String.valueOf(pnuNo));
      }
      if (jsonObject.containsKey("species_epithet"))
        e.setOriginalTermInfo("species_epithet", jsonObject.getString("species_epithet"));
      if (jsonObject.containsKey("subspecies_epithet"))
        e.setOriginalTermInfo("subspecies_epithet",
            jsonObject.get("subspecies_epithet").toString());
      if (jsonObject.containsKey("genus"))
        e.setOriginalTermInfo("genus", jsonObject.getString("genus"));
      if (jsonObject.containsKey("familia"))
        e.setOriginalTermInfo("familia", jsonObject.getString("familia"));
      if (jsonObject.containsKey("classis"))
        e.setOriginalTermInfo("classis", jsonObject.getString("classis"));
      if (jsonObject.containsKey("phylum"))
        e.setOriginalTermInfo("phylum", jsonObject.getString("phylum"));
      if (jsonObject.containsKey("regio"))
        e.setOriginalTermInfo("regio", jsonObject.getString("regio"));
      if (jsonObject.containsKey("authors"))
        e.setOriginalTermInfo("authors", jsonObject.getString("authors"));
      if (jsonObject.containsKey("reference"))
        e.setOriginalTermInfo("reference", jsonObject.getString("reference"));
      if (jsonObject.containsKey("comment"))
        e.setOriginalTermInfo("comment", jsonObject.get("comment").toString());
      if (jsonObject.containsKey("synonyms")) {
        JsonArray synonymsArray = jsonObject.getJsonArray("synonyms");
        String synonymsString = "";
        String synonym;
        for (int i = 0; i < synonymsArray.size(); i++) {
          JsonObject jo = synonymsArray.getJsonObject(i);
          synonym = jo.getString("synonym").replace("</i>", "").replace("<i>", "");
          // synonym = synonymsArray.getString(i).replace("</i>", "").replace("<i>", "");
          synonymsString = synonymsString + " " + synonym;
        }
        e.setOriginalTermInfo("synonyms", synonymsString.trim());
      }
      if (jsonObject.containsKey("lpsn"))
        e.setOriginalTermInfo("lpsn", jsonObject.getString("lpsn"));
      if (jsonObject.containsKey("wink_compendium"))
        e.setOriginalTermInfo("wink_compendium", jsonObject.get("wink_compendium").toString());
      if (jsonObject.containsKey("dsmz_catalogue"))
        e.setOriginalTermInfo("dsmz_catalogue", jsonObject.get("dsmz_catalogue").toString());
      if (jsonObject.containsKey("bacdive"))
        e.setOriginalTermInfo("bacdive", jsonObject.getString("bacdive"));
      rs.addEntry(e);
      return rs;
    } catch (Exception ex) {
      LOGGER.error("API exception in PNU term infos: " + ex.getMessage());
      return rs;
    }
  }

  /**
   * Implements the getTermInfosCombined method of the WSInterface Gets the informations of a taxon
   * given its URI with TS attributes when mapped and original attributes when not
   * 
   * @return GFBioResultSet
   */
  @Override
  public GFBioResultSet<TermCombinedResultSetEntry> getTermInfosCombined(String term_uri) {
    try {

      if (term_uri.startsWith("http:"))
        term_uri = term_uri.replace("http", "https");

      if (!term_uri.startsWith("http"))
        term_uri = uri + "species/" + term_uri + "/";
      // WebResource webResource = client.resource(term_uri);
      // ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
      Response response = client.target(term_uri).request(MediaType.APPLICATION_JSON).get();

      if (response.getStatus() != 200) {
        throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
      }
      GFBioResultSet<TermCombinedResultSetEntry> rs =
          new GFBioResultSet<TermCombinedResultSetEntry>("pnu");
      TermCombinedResultSetEntry e = new TermCombinedResultSetEntry();
      JsonReader jsonReader =
          Json.createReader(new StringReader(response.readEntity(String.class)));
      JsonObject jsonObject = jsonReader.readObject();
      e.setLabel(jsonObject.getString("label"));
      e.setRank(jsonObject.getString("taxon"));
      e.setUri(jsonObject.getString("url"));
      e.setSourceTerminology("PNU");
      if (jsonObject.containsKey("pnu_no")) {
        int pnuNo = jsonObject.getInt("pnu_no");
        e.setExternalID(String.valueOf(pnuNo));
      }
      if (jsonObject.containsKey("synonyms")) {
        JsonArray synonymsArray = jsonObject.getJsonArray("synonyms");
        String synonymsString = "";
        String synonym;
        for (int i = 0; i < synonymsArray.size(); i++) {
          JsonObject jo = synonymsArray.getJsonObject(i);
          synonym = jo.getString("synonym").replace("</i>", "").replace("<i>", "");
          synonymsString = synonymsString + " " + synonym;
        }
        e.setSynonym(synonymsString.trim());
      }
      if (jsonObject.containsKey("species_epithet"))
        e.setOriginalTermInfo("species_epithet", jsonObject.getString("species_epithet"));
      if (jsonObject.containsKey("subspecies_epithet"))
        e.setOriginalTermInfo("subspecies_epithet",
            jsonObject.get("subspecies_epithet").toString());
      if (jsonObject.containsKey("genus"))
        e.setOriginalTermInfo("genus", jsonObject.getString("genus"));
      if (jsonObject.containsKey("familia"))
        e.setOriginalTermInfo("familia", jsonObject.getString("familia"));
      if (jsonObject.containsKey("classis"))
        e.setOriginalTermInfo("classis", jsonObject.getString("classis"));
      if (jsonObject.containsKey("phylum"))
        e.setOriginalTermInfo("phylum", jsonObject.getString("phylum"));
      if (jsonObject.containsKey("regio"))
        e.setOriginalTermInfo("regio", jsonObject.getString("regio"));
      if (jsonObject.containsKey("authors"))
        e.setOriginalTermInfo("authors", jsonObject.getString("authors"));
      if (jsonObject.containsKey("reference"))
        e.setOriginalTermInfo("reference", jsonObject.getString("reference"));
      if (jsonObject.containsKey("comment"))
        e.setOriginalTermInfo("comment", jsonObject.get("comment").toString());
      if (jsonObject.containsKey("lpsn"))
        e.setOriginalTermInfo("lpsn", jsonObject.getString("lpsn"));
      if (jsonObject.containsKey("wink_compendium"))
        e.setOriginalTermInfo("wink_compendium", jsonObject.get("wink_compendium").toString());
      if (jsonObject.containsKey("dsmz_catalogue"))
        e.setOriginalTermInfo("dsmz_catalogue", jsonObject.get("dsmz_catalogue").toString());
      if (jsonObject.containsKey("bacdive"))
        e.setOriginalTermInfo("bacdive", jsonObject.getString("bacdive"));
      rs.addEntry(e);
      return rs;

    } catch (Exception ex) {
      LOGGER.error("API exception in PNU term infos: " + ex.getMessage());
      return new GFBioResultSet<TermCombinedResultSetEntry>("pnu");
    }
  }

  @Override
  public boolean isResponding() {
    // WebResource webResource = client.resource(uri);
    // ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
    Response response = client.target(uri).request(MediaType.APPLICATION_JSON).get();
    if (response.getStatus() != 200) {
      return false;
    }
    return true;
  }

  @Override
  public boolean supportsMatchType(String match_type) {
    return Arrays.stream(pnuConfig.getSearchModes())
        .anyMatch(SearchModes.valueOf(match_type)::equals);
  }

  /**
   * Implements the getCapabilities method of the WSInterface Returns the available service
   * endpoints for the webservice
   * 
   * @return GFBioResultSet
   */
  public GFBioResultSet<CapabilitiesResultSetEntry> getCapabilities() {
    GFBioResultSet<CapabilitiesResultSetEntry> rs =
        new GFBioResultSet<CapabilitiesResultSetEntry>("geonames");
    CapabilitiesResultSetEntry e = new CapabilitiesResultSetEntry();
    ArrayList<String> servicesArray = new ArrayList<String>();
    for (Services s : pnuConfig.getAvailableServices()) {
      servicesArray.add(s.toString());
    }
    e.setAvailableServices(servicesArray);

    ArrayList<String> modesArray = new ArrayList<String>();
    for (SearchModes m : pnuConfig.getSearchModes()) {
      modesArray.add(m.toString());
    }
    e.setSearchModes(modesArray);
    rs.addEntry(e);
    return rs;
  }

  /**
   * 
   * @param method
   * @param uri
   * @return
   */
  private Response buildRequest(String method, String termUri) {

    return client.target(uri + method + termUri).request(MediaType.APPLICATION_JSON).get();
  }
}
