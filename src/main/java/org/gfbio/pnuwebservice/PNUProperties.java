/**
 * 
 */
package org.gfbio.pnuwebservice;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Florian Becker
 *
 */
public class PNUProperties {

	private String username, password, uri;

	private static final String filename = "pnuws.properties";

	private static PNUProperties instance;

	private PNUProperties() {
		readProperties();
	}

	public static PNUProperties getInstance() {
		if (PNUProperties.instance == null) {
			PNUProperties.instance = new PNUProperties();
		}
		return PNUProperties.instance;
	}

	public void readProperties() {

		// try ... with automatically closes resources after use
		// searches for the file in the classpath, i.e., /src/main/resources
		try (InputStream input = PNUProperties.class.getClassLoader().getResourceAsStream(filename)) {

			Properties prop = new Properties();

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			username = prop.getProperty("username");
			password = prop.getProperty("password");
			uri = prop.getProperty("uri");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getUri() {
		return uri;
	}
}
