package org.gfbio.pnuwebservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import javax.json.JsonObject;
import org.gfbio.resultset.GFBioResultSet;
import org.gfbio.resultset.MetadataResultSetEntry;
import org.gfbio.resultset.SearchResultSetEntry;
import org.gfbio.resultset.SynonymsResultSetEntry;
import org.gfbio.resultset.TermCombinedResultSetEntry;
import org.gfbio.resultset.TermOriginalResultSetEntry;
import org.junit.Test;

public class PNUWSTest {

  @Test
  public void testSupportedMethods() {
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();
    assertTrue(serviceIMPL.supportsMatchType("exact"));
    assertTrue(serviceIMPL.supportsMatchType("included"));
  }

  @Test
  public void WSMetadata() {
    System.out.println("*** Test Metadata service ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();
    GFBioResultSet<MetadataResultSetEntry> check = serviceIMPL.getMetadata();
    assertNotNull(check);
    for (JsonObject j : check.create()) {
      System.out.println(j.toString());
    }
  }

  @Test
  public void isResponding() {
    System.out.println("*** Test isResponding ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();
    boolean check = serviceIMPL.isResponding();
    assertTrue(check);
  }

  @Test
  public void testSearchIncluded() {
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();
    GFBioResultSet<SearchResultSetEntry> check = serviceIMPL.search("Enterococcus", "included");
    assertNotNull(check);
    assertEquals("Enterococcus", check.create().get(0).getString("label"));
    assertEquals("Bacteria", check.create().get(0).getString("kingdom"));
    assertEquals("517033", check.create().get(0).getString("externalID"));
  }

  @Test
  public void WSSearchExactTest() {
    System.out.println("*** Test search exact service ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();
    GFBioResultSet<SearchResultSetEntry> check =
        serviceIMPL.search("Gluconacetobacter maltaceti", "exact");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSSearchIncludedTest() {
    System.out.println("*** Test search included service ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();

    GFBioResultSet<SearchResultSetEntry> check =
        serviceIMPL.search("acetobacter aceti", "included");

    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermCombined() {
    System.out.println("*** Test combined term service ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();

    GFBioResultSet<TermCombinedResultSetEntry> check =
        serviceIMPL.getTermInfosCombined("https://bacdive.dsmz.de/api/pnu/species/791752/");

    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WStermOriginal() {
    System.out.println("*** Test original term service ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();

    GFBioResultSet<TermOriginalResultSetEntry> check =
        serviceIMPL.getTermInfosOriginal("https://bacdive.dsmz.de/api/pnu/genus/514991/");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }

  @Test
  public void WSSynonyms() {
    System.out.println("*** Test synonym service ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();
    GFBioResultSet<SynonymsResultSetEntry> check = serviceIMPL.getSynonyms("791752");
    assertNotNull(check);
    System.out.println(check.create().toString());
  }

  @Test
  public void WStermProcessed() {
    System.out.println("*** Test processed term service ***");
    PNUWebserviceImpl serviceIMPL = new PNUWebserviceImpl();

    GFBioResultSet<SearchResultSetEntry> check =
        serviceIMPL.getTermInfosProcessed("https://bacdive.dsmz.de/api/pnu/species/791752/");
    assertNotNull(check);
    for (JsonObject a : check.create()) {
      System.out.println(a.toString());
    }
  }
}
